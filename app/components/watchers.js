Vue.component('watchers', {
    data() {
        return {
            user: null,
            oldUser: null
        }
    },
    methods: {
        async randomUser(){
            try{
                const data = await fetch('https://randomuser.me/api/')
                const json = await data.json();
                const user = json.results[0];
                this.user = `${user.name.title} ${user.name.first} ${user.name.last}`;
            }catch(e){
                // algo anda mal
            }
        }
    },
    watch: {
        user(newVal, oldVal){
            this.user = newVal;
            this.oldUser = oldVal;
        }
    },
    template: `
        <div>
            <h2>Whatchers</h2>
            <button @click="randomUser">Cargar</button>
            <p>Nuevo Usuario: {{ user }}</p>
            <p>Antiguo Usuario: {{ oldUser }}</p>
        </div>
    `
});