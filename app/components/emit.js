Vue.component('emit', {
    data(){
        return{
            carBrand: 'Chevrolet'
        }
    },
    template: `
        <div>
            <h2>Emitir Eventos</h2>
            <p @click="$emit('show_car_brand', carBrand)">
                Pulsa para el vento
            </p>
        </div>
    `
});