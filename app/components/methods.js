Vue.component('methods',{
    data(){
        return{
            name: 'Alejandro',
            surname: 'Sabas'
        }
    },
    computed: {
        fullName(){
            return `${this.name} ${this.surname}`
        }
    },
    methods: {
        hello(){
            alert(this.fullName);
        }
    },
    template:`
        <div>
            <h2>Métodos</h2>
            <p @click="hello">Pulsa aquí</p>
        </div>
    `
}); 