Vue.component('vmodel', {
    data() {
        return{
            framework: 'Vuejs 2'
        }
    },
    template:`
        <div>
            <h2>Vmodel</h2>
            <input v-model="framework" v-focus/>
            <p>framework: {{ framework }}</p>
        </div>
    `
});