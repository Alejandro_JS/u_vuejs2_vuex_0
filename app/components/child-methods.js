Vue.component('child-methods', {
    data (){
        return{
            cmpName: 'Child Methods'
        }
    },
    methods: {
        showCmpName(){
            console.log(this.cmpName);
        }
    },
    template:`
        <div>
            <h2>Acceso a métodos del cmp hijo</h2>
        </div>
    `
});