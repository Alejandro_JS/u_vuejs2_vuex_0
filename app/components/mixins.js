let myMixin = {
    mounted (){
        console.log("MIXIN init");
        console.log(this.mixinData);
        this.test();
    },
    data (){
        return {
            mixinData: 'Mixin Data'
        }
    },
    methods: {
        test() {
            console.log("test from mixins");
        }
    }
};
Vue.component('mixins',{
    mixins: [myMixin],
    mounted (){
        console.log("Mounted from component with mixins");
    },
    data (){
        return {
            mixinData: 'Mixin Data from Data'
        }
    },
    template: `
        <div>
            <h2>Mixins</h2>
            <p>{{ mixinData }}</p>
        </div>
    `
});