Vue.component('vmodel-checkboxes', {
    data(){
        return{
            frameworks:[]
        }
    },
    template: `
        <div>
            <h2>V-Model - Array</h2>
            <input type="checkbox" id="vuejs2" value="VueJS 2" v-model="frameworks"/>
            <label for="vuejs2">VueJS 2</label>
            <input type="checkbox" id="reactjs" value="React JS" v-model="frameworks"/>
            <label for="reactjs">React JS</label>
            <input type="checkbox" id="angularjs" value="Angular JS" v-model="frameworks"/>
            <label for="angularjs">Angular JS</label>
            <p>Framworks: {{ frameworks }}</p>
        </div>
    `
});